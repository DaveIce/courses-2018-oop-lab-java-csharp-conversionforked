package it.unibo.oop.util;

public interface Tuple2<A, B> extends Tuple {
	A getFirst();
    B getSecond();
}

﻿using System;
using System.Linq;

namespace Unibo.Oop.Utils
{
    internal class TupleImpl : ITuple
    {
        private Object[] list;
        public TupleImpl(object[] args)
        {
            this.list = args;
        }

        public object this[int i]
        {
            get
            {
                return this.list[i];
            }
        }

        public int Length
        {
            get
            {
                return this.list.Length;
            }
        }
        public object[] ToArray()
        {
            return this.list;
        }

        public override bool Equals(object obj)
        {
            if (obj is TupleImpl)
            {
                return Enumerable.SequenceEqual(this.list, ((TupleImpl)obj).list);
            }
            return false;
        }

        public override int GetHashCode()
        {
            int code = 0;
            foreach (var item in this.list)
            {
                code = code + item.GetHashCode();
            }
            return code;
        }

        public override string ToString()
        {
            if (this.Length == 1)
            {
                return $"(" + this[0] + ")";
            }

            if (this.Length == 2)
            {
                return string.Format("({0}, {1})", this[0], this[1]);
            }

            if (this.Length == 3)
            {
                return $"({this[0]}, {this[1]}, {this[2]})";
            }
            throw new System.NotImplementedException();
        }
    }
}
